package ir.android.mehdi.mehrapp.memoPass;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import ir.android.mehdi.mehrapp.R;


public class AccountListAdapter extends BaseAdapter {

    Context mContex;
    private ArrayList<AccountEntity> accounts;

    public AccountListAdapter(Context mContex) {
        this.mContex = mContex;
    }

    @Override
    public int getCount() {
        if (accounts != null) {
            return accounts.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return accounts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    void setAccounts(ArrayList<AccountEntity> accounts) {
        this.accounts = new ArrayList<>();
        this.accounts.addAll(accounts);
        notifyDataSetChanged();
    }

    public void clear() {
        ArrayList<AccountEntity> empty = new ArrayList<>();
        this.accounts.clear();
        this.accounts.addAll(empty);
        notifyDataSetInvalidated();
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        View v = LayoutInflater.from(mContex)
                .inflate(R.layout.account_list_item, viewGroup, false);

        TextView list = v.findViewById(R.id.list);
        list.setText(accounts.get(position).getListAccount());
        return v;
    }
}
