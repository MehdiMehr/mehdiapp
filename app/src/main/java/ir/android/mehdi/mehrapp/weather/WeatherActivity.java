package ir.android.mehdi.mehrapp.weather;

import cz.msebera.android.httpclient.Header;
import ir.android.mehdi.mehrapp.BaseActivity;
import ir.android.mehdi.mehrapp.PublicMethods;
import ir.android.mehdi.mehrapp.R;
import ir.android.mehdi.mehrapp.weather.models.Forecast;
import ir.android.mehdi.mehrapp.weather.models.Yahoo;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class WeatherActivity extends BaseActivity {
    ImageView flag ,todayIcon ;
    TextView city ,IP ,temp;
    ListView forcastsListView;
    String url ="https://api.ipgeolocation.io/ipgeo?apiKey=bb94d48a2a3843b0a9462ba06ffdae23";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        bind();
        getLocation();
        Load();
    }
    private void bind() {
        flag= findViewById(R.id.flag);
        city= findViewById(R.id.city);
        IP= findViewById(R.id.ipValue);
        temp= findViewById(R.id.temp);
        todayIcon= findViewById(R.id.todayIcon);
        forcastsListView= findViewById(R.id.forcastsListView);

        findViewById(R.id.reload ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Load();
            }
        });
    }

    private void Load() {
        dialog.show();
        getLocation();

    }


    /**
     *
     * Get  weather forcast from YAHOO!
     *
     */

    private void getWeatherForcast(String city) {

        AsyncHttpClient client = new AsyncHttpClient();
        String url_weather = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22"+city+"%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        client.get(url_weather, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                PublicMethods.toast(mContext,"Error in get IP");
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
              //  PublicMethods.toast(mContext,responseString);
                parseAndShow(responseString);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                dialog.dismiss();
            }
        });

    }


    void parseAndShow(String serverResponse){
        Gson gson = new Gson();
        Yahoo yahoo =gson.fromJson(serverResponse,Yahoo.class);
      if( yahoo.getQuery().getCount() ==1 ) {
          String temp = yahoo.getQuery().getResults()
                  .getChannel().getItem().getCondition().getTemp();
          this.temp.setText(temp);
          List< Forecast> forecasts =yahoo.getQuery().getResults()
                  .getChannel().getItem().getForecast();
          ForcastsListAdapter adapter = new ForcastsListAdapter(mContext,forecasts );
          forcastsListView.setAdapter(adapter);



      }else {
          PublicMethods.toast(mContext,"City Not found");

        }


    }

    /**
     *
     * Get  Location
     *
     */
    private void getLocation() {

        AsyncHttpClient asyncHttpClient =new AsyncHttpClient();
        asyncHttpClient.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                PublicMethods.toast(mContext,"Error in Get IP");

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
              //  Toast.makeText(WeatherActivity.this, responseString, Toast.LENGTH_SHORT).show();

                parsData(responseString);

            }
        });

    }

    void parsData(String response){
        try {
            JSONObject jsonObject =new JSONObject(response);
            String ipValue = jsonObject.getString("ip");
            String cityValue = jsonObject.getString("country_capital");
            String flagValue = jsonObject.getString("country_flag");

            IP.setText("IP :"+ ipValue);
            city.setText(cityValue);
            Glide.with(this)
                    .load(flagValue).into(flag);
            getWeatherForcast(cityValue);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
