package ir.android.mehdi.mehrapp;

import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class SharedPreferenceActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText txtName , txtFamily ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preference);
        Bind();
        LoadData();


    }

    private void LoadData() {

        String Name = PreferenceManager.getDefaultSharedPreferences(this)
                .getString("Name" ,"");
        String Family = PreferenceManager.getDefaultSharedPreferences(this)
                .getString("Family" ,"");
        this.txtName.setText(Name);
        this.txtFamily.setText(Family);


    }

    private void Bind() {
        txtName=findViewById(R.id.txtName);
        txtFamily=findViewById(R.id.txtfamily);
        findViewById(R.id.btnSave).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        save(txtName.getText().toString()
                ,
                txtFamily.getText().toString()
                );



    }

    void save(String userName , String userFamily){
        PreferenceManager.getDefaultSharedPreferences(this)
                .edit().putString("Name",userName).apply();
        PreferenceManager.getDefaultSharedPreferences(this)
                .edit().putString("Family",userFamily).apply();
        cleanForm();
        


    }

    private void cleanForm() {
        txtName.setText(" ");
        txtFamily.setText(" ");
        Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
    }
}
