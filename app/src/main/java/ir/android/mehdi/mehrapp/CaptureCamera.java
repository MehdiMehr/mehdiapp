package ir.android.mehdi.mehrapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class CaptureCamera extends AppCompatActivity {

    private Button btnCamera;
    private ImageView imgCamera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture_camera);
        bind();
        takePicture();

    }

    private void takePicture() {
        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent,9000);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==9000){
            if (requestCode==Activity.RESULT_OK){
                Bundle extras = data.getExtras();
                Bitmap imgBitmap = (Bitmap) extras.get("data");

                Glide.with(this)
                        .load(imgBitmap)
                        .into(imgCamera);


            }



        }
    }

    private void bind() {

        btnCamera = findViewById(R.id.btnCamera);
        imgCamera =findViewById(R.id.imgCamera);
    }
}
