package ir.android.mehdi.mehrapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ViewDebug;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class MainActivity extends AppCompatActivity {
    ImageView img  ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imgLoader();


    }

    private void imgLoader() {

        img=findViewById(R.id.img);
        Glide.with(this)
                .load("http://profilepicturesdp.com/wp-content/uploads/2018/07/profile-picture-avatars-5.png")
                .into(img);


    }
}
