package ir.android.mehdi.mehrapp.memoPass;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;

class MyDatabaseHelper extends SQLiteOpenHelper {

    public static final String TBL_ACCOUNT = "account";


    public MyDatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {


        db.execSQL("CREATE TABLE " + TBL_ACCOUNT + " (" +
                "ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "ACCOUNT  TEXT    NOT NULL, " +
                "PASSWORD  TEXT    NOT NULL ) "
        );
    }

    void addUser(String Account, String Password) {

        String querry = "INSERT INTO " + TBL_ACCOUNT + "(ACCOUNT , PASSWORD) " +
                "VALUES('" + Account + "' , '" + Password + "')";

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(querry);
        db.close();
    }

    String getUsers() {
        String results = "";
        String query = " SELECT ID,ACCOUNT,PASSWORD FROM " + TBL_ACCOUNT;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            results += cursor.getString(0) + " "
                    + cursor.getString(1) + "\n";

        }

        return results;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS  " + TBL_ACCOUNT);
        onCreate(db);

    }
    /**
     *
     * Retrieve Data From DataBase in ArrayList
     *
     **/
    public ArrayList<AccountEntity> dblistHandler() {
        ArrayList<AccountEntity> dbList = new ArrayList<>();

        String query1 = " SELECT ID,ACCOUNT,PASSWORD FROM " + TBL_ACCOUNT;
        SQLiteDatabase dbQuery = this.getReadableDatabase();
        Cursor cursor = dbQuery.rawQuery(query1, null);

        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            String idList = cursor.getString(0);
            String accountList = cursor.getString(1);
            String passwordList = cursor.getString(2);
            dbList.add(new AccountEntity(idList, accountList, passwordList));
        }

        return dbList;

    }

}
