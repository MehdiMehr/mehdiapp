package ir.android.mehdi.mehrapp.memoPass;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import ir.android.mehdi.mehrapp.R;

public class MemoPassActivity extends Activity implements View.OnClickListener {
    EditText account, password;
    TextView results;
    MyDatabaseHelper db;
    ListView accountListView;
    AccountListAdapter adapter;
    //String accounts[] = {"sahar","anna","lisa","syndy","simon","lisa","syndy","simon"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memo_pass);

        /**
         * Creat a dataBase
         */
        db = new MyDatabaseHelper(this, "memopass.db", null, 2);
        bind();
        SetListView();
        loadData();
    }

    /**
     * Creat  ListView
     */
    private void SetListView() {
        adapter = new AccountListAdapter(this);
        accountListView.setAdapter(adapter);

    }

    private void bind() {
        account = findViewById(R.id.account);
        password = findViewById(R.id.password);
        results = findViewById(R.id.results);
        accountListView = findViewById(R.id.accountListView);
        findViewById(R.id.save).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        save(account.getText().toString(), password.getText().toString());
    }

    private void save(String account, String password) {
        db.addUser(account, password);
        adapter.clear();
        loadData();
    }

    void loadData() {
        //results.setText(db.getUsers());
        adapter.setAccounts(db.dblistHandler());
    }
}
