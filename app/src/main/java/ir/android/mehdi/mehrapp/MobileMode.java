package ir.android.mehdi.mehrapp;


import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;


public class MobileMode extends AppCompatActivity {
    private Button btnWifi ,btnBluetooth;
    private Switch wifi_switch ,bluetooth_switch ;
    private WifiManager wifi_manager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_mode);

        bind();
        wifiStatus();
        bluetoothStatus();




    }





    /**
     * WiFi Intent
     *
     */
    private void wifiStatus() {

        //------< WiFi Intent >-------

        btnWifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent wifiIntent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                startActivityForResult(wifiIntent, 1000);
            }
        });
        //---------< Wifi Check >---------
        wifi_manager  =(WifiManager)

                getApplicationContext().

                        getSystemService(Context.WIFI_SERVICE);

        wifi_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()

        {
            @Override
            public void onCheckedChanged (CompoundButton buttonView,boolean isChecked){
                if (isChecked) {
                    wifi_manager.setWifiEnabled(true);
                    wifi_switch.setText("WiFi is ON");
                    Toast.makeText(MobileMode.this, "Wifi is On", Toast.LENGTH_SHORT).show();
                } else {
                    wifi_manager.setWifiEnabled(false);
                    wifi_switch.setText("WiFi is OFF");
                    Toast.makeText(MobileMode.this, "Wifi is OFF", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    /**
     * Bluetooth Intent
     *
     *
     */
    private void bluetoothStatus() {

        //------< WiFi Intent >-------

        btnBluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent bluetoothIntent = new Intent(Settings.ACTION_BLUETOOTH_SETTINGS);
                startActivityForResult(bluetoothIntent,2000);
            }
        });




    }



    /**
     * On Activity Result Of:
     * WIFI
     * Bluetooth
     *
     */

        @Override
        protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == 1000) {
                if (wifi_manager.isWifiEnabled()) {
                    wifi_switch.setChecked(true);
                    wifi_switch.setText("WiFi is ON");
                    Toast.makeText(MobileMode.this, "WiFi is ON", Toast.LENGTH_SHORT).show();
                } else {
                    wifi_switch.setChecked(false);
                    wifi_switch.setText("WiFi is OFF");
                   // Toast.makeText(MobileMode.this, "WiFi is OFF", Toast.LENGTH_SHORT).show();
                }


            }

        }

    /**
     * Bind :
     * wifi switch and button
     * Bluetooth
     *
     */
    private void bind() {
        btnWifi  = findViewById(R.id.btnWifi);
        wifi_switch =findViewById(R.id.wifi_switch);
        btnBluetooth =findViewById(R.id.btnBluetooth);
        bluetooth_switch = findViewById(R.id.wifi_switch);

    }

}



