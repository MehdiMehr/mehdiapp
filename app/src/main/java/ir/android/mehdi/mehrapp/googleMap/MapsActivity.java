package ir.android.mehdi.mehrapp.googleMap;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.TextView;


import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.sql.Time;
import java.util.Date;
import java.util.TimeZone;

import cz.msebera.android.httpclient.Header;
import ir.android.mehdi.mehrapp.PublicMethods;
import ir.android.mehdi.mehrapp.R;
import ir.android.mehdi.mehrapp.weather.models.Yahoo;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnCameraIdleListener {

    private GoogleMap mMap;
    TextView details ;
    String time ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        bind();
//        onLoad();

    }

//
//   private void onLoad() {
//        TimeZone tz = TimeZone.getDefault();
//
////       if(tz.inDaylightTime()==false ) {
////           mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style));
//     }

    private void bind() {
        details= findViewById(R.id.details);
    }
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng Mirdamad = new LatLng(35.760151, 51.433899);
        mMap.getUiSettings().setZoomControlsEnabled(true);


        // Add a marker in Sydney and move the camera
//        mMap.addMarker(new MarkerOptions().position(Mirdamad).title("Marker in Mirdamad"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(Mirdamad));

        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(Mirdamad, 15);
        mMap.moveCamera(location);
        mMap.setOnCameraIdleListener(this);
    }

    @Override
    public void onCameraIdle() {
        double latitude = mMap.getCameraPosition().target.latitude;
        double longitude = mMap.getCameraPosition().target.longitude;
        PublicMethods.toast(this, "lat : " + latitude + ", Long : " + longitude);
        getWeatherByLocation(latitude, longitude);

    }

    void getWeatherByLocation(double lat, double lon) {
        AsyncHttpClient client = new AsyncHttpClient();
        String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(SELECT%20woeid%20FROM%20geo.places%20WHERE%20text%3D%22(" + lat + "," + lon + ")%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                PublicMethods.toast(MapsActivity.this, "Error in get Weather");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseAndShow(responseString);
            }


        });
    }

    void parseAndShow(String serverResponse) {
        Gson gson = new Gson();
        Yahoo yahoo = gson.fromJson(serverResponse, Yahoo.class);
        if (yahoo.getQuery().getCount() == 1) {
            String city = yahoo.getQuery().getResults()
                    .getChannel().getLocation().getCity();
            String country = yahoo.getQuery().getResults()
                    .getChannel().getLocation().getCountry();

            String temp = yahoo.getQuery().getResults()
                    .getChannel().getItem().getCondition().getTemp();
            String time =yahoo.getQuery().getResults().getChannel().getLastBuildDate();

            details.setText("You are here : \n"+country+" - "+city+ "\nTemp is   :"+temp+"  F \n"+time);

        }

    }

    }


