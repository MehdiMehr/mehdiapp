package ir.android.mehdi.mehrapp.weather;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import java.util.List;
import ir.android.mehdi.mehrapp.R;
import ir.android.mehdi.mehrapp.weather.models.Forecast;

public class ForcastsListAdapter extends BaseAdapter {
    Context mContext;
    List<Forecast> forcast;

    public ForcastsListAdapter(Context mContext, List<Forecast> forcast) {
        this.mContext = mContext;
        this.forcast = forcast;
    }

    @Override
    public int getCount() {
        return forcast.size();
    }

    @Override
    public Object getItem(int position) {
        return forcast.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
            View v = LayoutInflater.from(mContext)
                    .inflate(R.layout.forcasts_list_item,viewGroup,false);

        Forecast forecast = forcast.get(position);

        ImageView icon =v.findViewById(R.id.icon);
        TextView day =v.findViewById(R.id.day);
        TextView temp =v.findViewById(R.id.temp);
        TextView text =v.findViewById(R.id.text);


        day.setText(forecast.getDay());
        temp.setText(forecast.getLow());
        text.setText(forecast.getText());

        forcastCode(forecast.getCode() ,icon );

        return v;
    }


    void forcastCode(String code ,ImageView icon ){
        switch (code){
            case "26" : Glide.with(mContext).load("https://cdn.aerisapi.com/wxicons/v2/cloudy.png").into(icon);
                break;
            case "28" : Glide.with(mContext).load("https://cdn.aerisapi.com/wxicons/v2/mcloudysfw.png").into(icon);
                break;
            case "34" : Glide.with(mContext).load("https://cdn.aerisapi.com/wxicons/v2/clear.png").into(icon);
                break;

            case "30" : Glide.with(mContext).load("https://cdn.aerisapi.com/wxicons/v2/fair.png").into(icon);
                break;




        }


    }
}
