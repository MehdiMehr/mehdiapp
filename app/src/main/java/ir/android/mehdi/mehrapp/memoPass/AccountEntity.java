package ir.android.mehdi.mehrapp.memoPass;

public class AccountEntity {

    private String listID, listAccount, listPassword;

    public String getListID() {
        return listID;
    }

    public void String(String listID) {
        this.listID = listID;
    }

    public String getListAccount() {
        return listAccount;
    }

    public void setListAccount(String listAccount) {
        this.listAccount = listAccount;
    }

    public String getListPassword() {
        return listPassword;
    }

    public void setListPassword(String listPassword) {
        this.listPassword = listPassword;
    }

    public AccountEntity(String id, String account, String password) {
        super();
        this.listID = id;
        this.listAccount = account;
        this.listPassword = password;

    }


}
